import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { initializeApollo } from '../../apollo/apollo';
import { NextPage } from 'next';
import AllIngredients from '../../components/features/AllIngredients';
import { useRouter } from 'next/dist/client/router';
import RecipeDetails from '../../components/features/RecipeDetails';
import Steps from '../../components/features/Steps';
import Head from 'next/head';

const RecipeQuery = gql`
    query RecipeQuery($id: ID!) {
        recipe(id: $id) {
            id
            name
            description
            servings
            yield
            totalMinutes
            tags
            createdBy {
                name
            }
            createdOn
            lastUpdated

            allIngredients {
                name
                measurement
            }

            steps {
                ingredients {
                    name
                    measurement
                }
                goal
                directions
                activeMinutes
                waitMinutes
            }
        }
    }
`;

const Index: NextPage = () => {
    const router = useRouter();
    const { data } = useQuery(RecipeQuery, {
        variables: { id: router.query.id },
    });
    const recipe = data!.recipe as Recipe;

    return (
        <>
            <Head>
                <title>{recipe.name} - scrapsbook</title>
            </Head>
            <RecipeDetails recipe={recipe} />
            <hr />
            <AllIngredients ingredients={recipe.allIngredients} />
            <hr />
            <Steps steps={recipe.steps} />
        </>
    );
};

export async function getStaticProps(ctx) {
    const apolloClient = initializeApollo();

    await apolloClient.query({
        query: RecipeQuery,
        variables: { id: ctx.params.id },
    });

    return {
        props: {
            initialApolloState: apolloClient.cache.extract(),
        },
    };
}

export async function getStaticPaths() {
    const apolloClient = initializeApollo();
    const result = await apolloClient.query({
        query: gql`
            {
                recipes {
                    id
                }
            }
        `,
    });
    return {
        paths: result.data.recipes.map((x: Recipe) => `/recipe/${x.id}`),
        fallback: false,
    };
}

export default Index;
