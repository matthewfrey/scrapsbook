const chocChipCookiesSteps: RecipeStep[] = [
    {
        goal: 'Prep',
        ingredients: [],
        directions: ['Preheat oven to 350F'],
    },
    {
        goal: 'Make Batter',
        ingredients: [
            { name: 'butter, softened', measurement: '1 cup' },
            { name: 'white sugar', measurement: '1 cup' },
            { name: 'brown sugar', measurement: '1 cup' },
            { name: 'eggs', measurement: '2' },
            { name: 'vanilla extract', measurement: '2 tsp' },
        ],
        directions: [
            'Cream together the butter, white sugar, and brown sugar until smooth.',
            'Beat in the eggs one at a time, then stir in the vanilla.',
        ],
        activeMinutes: 15,
    },
    {
        goal: 'Make Dough',
        ingredients: [
            { name: 'baking soda', measurement: '1 tsp' },
            { name: 'hot water', measurement: '2 tsp' },
            { name: 'salt', measurement: '0.5 tsp' },
            { name: 'flour', measurement: '3 cups' },
            { name: 'chocolate chips', measurement: '2 cups' },
            { name: 'chopped walnuts', measurement: '1 cup' },
        ],
        directions: [
            'Dissolve baking soda in hot water.',
            'Add to batter along with salt.',
            'Stir in flour, chocolate chips, and nuts.',
        ],
        activeMinutes: 15,
    },
    {
        goal: 'Bake',
        ingredients: [],
        directions: [
            'Drop by large spoonfuls onto ungreased pans.',
            'Bake for about 10 minutes in the preheated oven, or until edges are nicely browned.',
        ],
        activeMinutes: 10,
        waitMinutes: 10,
    },
];
export const CHOC_CHIP_COOKIES_RECIPE: Recipe = {
    id: '1',
    name: 'Best Chocolate Chip Cookies',
    description: 'Crisp edges, chewy middles.',
    servings: 24,
    yield: 48,
    activeMinutes: chocChipCookiesSteps.reduce(
        (prev, cur) => prev + (cur.activeMinutes || 0),
        0
    ),
    waitMinutes: chocChipCookiesSteps.reduce(
        (prev, cur) => prev + (cur.waitMinutes || 0),
        0
    ),
    totalMinutes: chocChipCookiesSteps.reduce(
        (prev, cur) => prev + (cur.activeMinutes || 0) + (cur.waitMinutes || 0),
        0
    ),
    tags: ['crispy', 'chewy'],
    allIngredients: chocChipCookiesSteps.reduce(
        (prev, cur) => [...prev, ...cur.ingredients],
        []
    ),
    steps: chocChipCookiesSteps,
    createdBy: {
        name: 'Ellen',
        id: 'u1',
    },
    createdOn: new Date().toISOString(),
    lastUpdated: new Date().toISOString(),
};
export default function recipes(_parent, _args, _context, _info): Recipe[] {
    return [CHOC_CHIP_COOKIES_RECIPE];
}
