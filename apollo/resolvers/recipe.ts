import { CHOC_CHIP_COOKIES_RECIPE } from './recipes';

export default function recipe() {
    return CHOC_CHIP_COOKIES_RECIPE;
}
