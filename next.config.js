const withPWA = require('next-pwa');

module.exports = withPWA({
    webpack(config, options) {
        config.module.rules.push({
            test: /\.graphql$/,
            exclude: /node_modules/,
            use: [
                options.defaultLoaders.babel,
                { loader: 'graphql-let/loader' },
            ],
        });

        config.module.rules.push({
            test: /\.graphqls$/,
            exclude: /node_modules/,
            use: ['graphql-tag/loader', 'graphql-let/schema/loader'],
        });

        return config;
    },
    pwa: {
        dest: 'public',
        disable: process.env.NODE_ENV !== 'production',
    },
});
