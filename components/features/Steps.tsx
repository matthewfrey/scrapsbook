import React from 'react';
import Ingredient from './Ingredient';

interface Props {
    steps: RecipeStep[];
}

const Steps: React.FC<Props> = ({ steps }) => {
    console.log(steps);
    return (
        <div>
            {steps.map((step, i) => (
                <div className="step" key={i}>
                    <h3 className="step-num">
                        {i + 1}: {step.goal}
                    </h3>
                    <ul className="ingredients">
                        {step.ingredients.map((x, i) => (
                            <li className="ingredient" key={i}>
                                <Ingredient ingredient={x} key={i} />
                            </li>
                        ))}
                    </ul>
                    <ol className="directions">
                        {step.directions.map((x, i) => (
                            <li className="direction" key={i}>
                                {x}
                            </li>
                        ))}
                    </ol>
                    {step.waitMinutes && (
                        <div className="waitMinutes">
                            Wait for {step.waitMinutes} minutes.
                        </div>
                    )}
                </div>
            ))}
        </div>
    );
};

export default Steps;
