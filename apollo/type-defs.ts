import gql from 'graphql-tag';

export default gql`
    type Recipe {
        # TODO: Add Images
        id: ID!
        name: String!
        description: String
        servings: Int
        yield: Int
        activeMinutes: Int
        waitMinutes: Int
        totalMinutes: Int
        tags: [String]
        createdBy: User!
        createdOn: String!
        lastUpdated: String!

        allIngredients: [Ingredient]!
        steps: [RecipeStep]!
    }

    type Ingredient {
        name: String!
        measurement: String!
    }

    type RecipeStep {
        ingredients: [Ingredient]!
        goal: String!
        directions: [String]!
        activeMinutes: Int
        waitMinutes: Int
    }

    type User {
        id: ID!
        name: String!
    }

    type Query {
        recipes: [Recipe]
        recipe(id: ID!): Recipe
    }
`;
