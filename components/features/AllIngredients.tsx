import React from 'react';
import { Button } from 'grommet/components/Button';
import Ingredient from './Ingredient';

interface Props {
    ingredients: Ingredient[];
}

const AllIngredients: React.FC<Props> = ({ ingredients }) => {
    const [hidden, setHidden] = React.useState(true);
    const toggleHidden = () => setHidden(!hidden);
    return (
        <div>
            {!hidden && (
                <ul>
                    {ingredients.map((ing, i) => (
                        <li key={i}>
                            <Ingredient ingredient={ing} />
                        </li>
                    ))}
                </ul>
            )}
            <Button
                onClick={toggleHidden}
                color="primary"
                label={hidden ? 'show ingredient list' : 'hide ingredient list'}
            />
        </div>
    );
};
export default AllIngredients;
