declare interface Recipe {
    id: string;
    name: string;
    description?: string;
    servings?: number;
    yield?: number;
    activeMinutes: number;
    waitMinutes: number;
    totalMinutes: number;
    tags: string[];

    allIngredients: Ingredient[];
    steps: RecipeStep[];

    createdBy: User;
    createdOn: string;
    lastUpdated: string;
}

declare interface Ingredient {
    name: string;
    measurement: string;
}

declare interface RecipeStep {
    ingredients: Ingredient[];
    directions: string[];
    goal: string;
    activeMinutes?: number;
    waitMinutes?: number;
}

declare interface User {
    id: string;
    name: string;
}
