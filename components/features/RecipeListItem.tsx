import React from 'react';
import Link from 'next/link';

interface Props {
    recipe: Recipe;
}

const RecipeListItem: React.FC<Props> = ({ recipe }) => {
    return (
        <div>
            <Link as={`/recipe/${recipe.id}`} href={`/recipe/[id]`}>
                <a>{recipe.name}</a>
            </Link>
        </div>
    );
};

export default RecipeListItem;
