import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { initializeApollo } from '../apollo/apollo';
import RecipeListItem from '../components/features/RecipeListItem';
import Link from 'next/link';
import { Button } from 'grommet/components/Button';

const RecipesQuery = gql`
    query RecipesQuery {
        recipes {
            id
            name
        }
    }
`;

const Index = () => {
    const { data } = useQuery(RecipesQuery);
    const { recipes } = data!;

    return (
        <>
            {recipes.map((recipe: Recipe) => (
                <RecipeListItem key={recipe.id} recipe={recipe} />
            ))}
            <Link href="/recipe/new">
                <Button label="+" />
            </Link>
        </>
    );
};

export async function getStaticProps() {
    const apolloClient = initializeApollo();

    await apolloClient.query({
        query: RecipesQuery,
    });

    return {
        props: {
            initialApolloState: apolloClient.cache.extract(),
        },
    };
}

export default Index;
