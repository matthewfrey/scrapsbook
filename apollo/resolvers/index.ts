import recipes from './recipes';
import recipe from './recipe';

const resolvers = {
    Query: {
        recipes,
        recipe,
    },
};

export default resolvers;
