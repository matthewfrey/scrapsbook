import React from 'react';

interface Props {
    ingredient: Ingredient;
}

const Ingredient: React.FC<Props> = ({ ingredient }) => {
    return (
        <>
            {ingredient.measurement}&nbsp;<b>{ingredient.name}</b>
        </>
    );
};

export default Ingredient;
