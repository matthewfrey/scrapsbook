import { AppProps } from 'next/app';
import { ApolloProvider } from '@apollo/react-hooks';
import { useApollo } from '../apollo/apollo';
import { Grommet } from 'grommet';
import GlobalStyles from '../components/global/GlobalStyles';
import Head from 'next/head';

export default function App({ Component, pageProps }: AppProps) {
    const apolloClient = useApollo(pageProps.initialApolloState);

    return (
        <ApolloProvider client={apolloClient}>
            <GlobalStyles />
            <Head>
                    <title>scrapsbook</title>
                <link
                    href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;500;700;900&display=swap"
                    rel="stylesheet"
                />
                    <link rel="icon" type="image/x-icon" href="/icon.png" />
            </Head>
            <Grommet
                theme={{
                    global: {
                        font: {
                            family: '"Work Sans", sans-serif',
                            size: '16px',
                            height: '20px',
                        },
                    },
                }}
            >
                <Component {...pageProps} />
            </Grommet>
        </ApolloProvider>
    );
}
