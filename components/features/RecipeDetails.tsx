import React from 'react';

interface Props {
    recipe: Recipe;
}

const RecipeDetails: React.FC<Props> = ({ recipe }) => {
    return (
        <>
            <h1 className="name">{recipe.name}</h1>
            <h2 className="description">{recipe.description}</h2>
            <ul className="details">
                {recipe.servings && (
                    <li className="servings">Serves: {recipe.servings}</li>
                )}
                {recipe.yield && (
                    <li className="yield">Yields: {recipe.yield}</li>
                )}
                {recipe.activeMinutes && (
                    <li className="activeMinutes">
                        Active time: {recipe.activeMinutes} minutes
                    </li>
                )}
                {recipe.waitMinutes && (
                    <li className="waitMinutes">
                        Idle time: {recipe.waitMinutes} minutes
                    </li>
                )}
                {(recipe.tags || []).length > 0 && (
                    <li className="tags">Tags: {recipe.tags.join(', ')}</li>
                )}
                <li className="createdBy">
                    Created by: {recipe.createdBy.name}
                </li>
                <li className="createdOn">Created on: {recipe.createdOn}</li>
                <li className="lastUpdated">
                    Last Updated: {recipe.lastUpdated}
                </li>
            </ul>
        </>
    );
};

export default RecipeDetails;
